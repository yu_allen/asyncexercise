﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace tryTask
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Test1
            string test1_result = string.Empty;
            var Test1 =Task.Factory.StartNew(() =>
            {
                test1_result = TestTask();
                Console.WriteLine(test1_result);
            });
            #endregion

            #region Test2
            string test2_result = string.Empty;
            var Test2 = Task.Factory.StartNew(() =>
            {
                test2_result = TestTask2();
                Console.WriteLine(test1_result);
            });
            #endregion
            //Task.WaitAny(Test1, Test2);
            //Console.WriteLine(string.Format("WaitAny({0})", DateTime.Now.ToString()));

            //Task.WaitAll(Test1, Test2);
            //Console.WriteLine(string.Format("WaitAll({0})", DateTime.Now.ToString()));

            #region Test3
            string test3_result = string.Empty;
            Task<string> Test3Async = TestTask3();
            test3_result = Test3Async.Result;
            Console.WriteLine(test3_result);
            #endregion

            #region Test4
            string test4_result = string.Empty;
            Task<string> Test4Async = TestTask4();
            test4_result = Test4Async.Result;
            Console.WriteLine(test4_result);
            #endregion

            Console.Read();
        }
        static string TestTask()
        {
            Thread.Sleep(3000);
            string showText = string.Format("TestTask Run! TIME({0})",DateTime.Now.ToString());
            Console.WriteLine(showText);
            return  "TestTask return Done!";
        }

        static string TestTask2()
        {
            Thread.Sleep(5000);
            string showText = string.Format("TestTask2 Run! TIME({0})", DateTime.Now.ToString());
            Console.WriteLine(showText);
            return "TestTask2 return Done!";
        }
        static async Task<string> TestTask3()
        {
            string txt = await Task.Run(()=> task3func());
            return txt;
        }
        static string task3func()
        {
            Thread.Sleep(7000);
            string showText = string.Format("TestTask3 Run! TIME({0})", DateTime.Now.ToString());
            Console.WriteLine(showText);

            return "TestTask3 return Done!";
        }

        static async Task<string> TestTask4()
        {
            string txt = await Task.Run(() => {
                Thread.Sleep(9000);
                string showText = string.Format("TestTask4 Run! TIME({0})", DateTime.Now.ToString());
                Console.WriteLine(showText);
                return "TestTask4 return Done!";
            });
            return txt;
        }
    }
}
